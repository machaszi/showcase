# Purpose of this repository
I have selected some of my recent code changes to my after hours project as a showcase for my coding skills.
On the one hand it is pretty hard to select a big piece of code which I could consider as a 100% of my own work, as I cowork very closely with a colleague. On the other hand I would like to avoid giving an access to our whole codebase phonically.

The following files presents some of my tasks:

 * Refactor Order class with help of composition-like design pattern in order to cover two functionalities with a common root: search results filtering (`ItemFilter.java`) and orders processing (`Order.java`)
 * Added a password reset functionality for a user (`UserController.java`, `Token.java`, `TokenDao.java`)
 * Security improvement by introducing bcrypt as a password hashing algorithm (`PasswordEncoder.java`)


