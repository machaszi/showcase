package com.lagomsoft.pearldiver.engine;

import com.lagomsoft.pearldiver.engine.categories.Category;

import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

/**
 * Filters Items lists based on search filters criteria
 */
public class ItemFilter {
    private final boolean includeClosedItems;
    private final ItemCriteria itemCriteria;

    private ItemFilter(Category category, String query, Integer minPrice, Integer maxPrice, Boolean includeDelivery, ItemCondition itemCondition, OfferType offerType, boolean includeClosedItems) {

        this.includeClosedItems = includeClosedItems;
        itemCriteria = new ItemCriteria(category, query, minPrice, maxPrice, includeDelivery,
                itemCondition, offerType);
    }


    public Optional<Category> getCategory() {
        return itemCriteria.getCategory();
    }

    public String getQuery() {
        return itemCriteria.getQuery();
    }

    public Optional<Integer> getMinPrice() {
        return itemCriteria.getMinPrice();
    }

    public Optional<Integer> getMaxPrice() {
        return itemCriteria.getMaxPrice();
    }

    public Optional<Boolean> getIncludeDelivery() {
        return itemCriteria.getIncludeDelivery();
    }

    public Optional<ItemCondition> getItemCondition() {
        return itemCriteria.getItemCondition();
    }

    public Optional<OfferType> getOfferType() {
        return itemCriteria.getOfferType();
    }

    public boolean isIncludeClosedItems() {
        return includeClosedItems;
    }

    public boolean matches(Item item) {
        return itemCriteria.matches(item);
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return itemCriteria.toStringHelper()
                .add("includeClosedItems", includeClosedItems)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ItemFilter itemFilter = (ItemFilter) o;

        //noinspection OverlyComplexBooleanExpression
        return itemCriteria.equals(itemFilter.itemCriteria)
                && Objects.equals(includeClosedItems, itemFilter.includeClosedItems);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(itemCriteria);
        result = 31 * result + (includeClosedItems ? 1 : 0);
        return result;
    }

    public static class Builder {
        private Category category;
        private String query;
        private Integer minPrice;
        private Integer maxPrice;
        private Boolean includeDelivery;
        private ItemCondition itemCondition;
        private OfferType offerType;
        private boolean includeClosedItems;

        private Builder() {
        }

        public Builder withMinPrice(Integer minPrice) {
            this.minPrice = minPrice;
            return this;
        }

        public Builder withMaxPrice(Integer maxPrice) {
            this.maxPrice = maxPrice;
            return this;
        }

        public Builder withIncludeDelivery(Boolean includeDelivery) {
            this.includeDelivery = includeDelivery;
            return this;
        }

        public Builder withItemCondition(ItemCondition itemCondition) {
            this.itemCondition = itemCondition;
            return this;
        }

        public Builder withOfferType(OfferType offerType) {
            this.offerType = offerType;
            return this;
        }

        public Builder withIncludeClosedItems(boolean includeClosedItems) {
            this.includeClosedItems = includeClosedItems;
            return this;
        }

        public Builder withCategory(Category category) {
            this.category = category;
            return this;
        }

        public Builder withQuery(String query) {
            this.query = query;
            return this;
        }

        public ItemFilter build() {
            return new ItemFilter(category, query, minPrice, maxPrice, includeDelivery,
                    itemCondition, offerType, includeClosedItems);
        }
    }
}
