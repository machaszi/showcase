package com.lagomsoft.pearldiver.engine.persistence;

import com.lagomsoft.pearldiver.engine.Token;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TokenDao {

    private static final String SELECT_FROM_TOKENS = "SELECT * FROM tokens";
    private final ConnectionFactory connectionFactory;
    private static final SecureRandom secureRandom = new SecureRandom();

    public TokenDao(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public void create(Token token) throws SQLException {
        try (Connection connection = connectionFactory.createConnection();
             PreparedStatement st = connection.prepareStatement(
                     "INSERT INTO tokens (" +
                             "id, requestor_id, expiration) " +
                             "VALUES (?, ?, ?)")) {
            int i = 1;
            st.setString(i++, token.getId());
            st.setString(i++, token.getRequestorId());
            st.setTimestamp(i++, getSqlTimestamp(token));
            st.executeUpdate();
        }
    }

    private void delete(String id) throws SQLException {
        try (Connection connection = connectionFactory.createConnection();
             PreparedStatement st = connection.prepareStatement(
                     "DELETE FROM tokens WHERE id = ?")) {
            st.setString(1, id);
            int deletedRecords = st.executeUpdate();

            if (deletedRecords != 1) {
                throw new SQLException("An error occurred while deleting tokens");
            }
        }
    }

    public void delete(Token token) throws SQLException {
        delete(token.getId());
    }

    public List<Token> find() throws SQLException {
        ArrayList<Token> users = new ArrayList<>();

        try (Connection connection = connectionFactory.createConnection();
             Statement st = connection.createStatement()) {
            try (ResultSet rs = st.executeQuery(SELECT_FROM_TOKENS)) {
                while (rs.next()) {
                    users.add(toToken(rs));
                }
            }
        }

        return users;
    }

    public Token findById(String id) throws SQLException {
        try (Connection connection = connectionFactory.createConnection();
             PreparedStatement st = connection.prepareStatement(
                     "SELECT id, requestor_id, expiration FROM tokens WHERE id = ?")) {
            st.setString(1, id);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return toToken(rs);
                } else {
                    throw new IllegalArgumentException(
                            String.format("Token with id %s doesn't exist.", id));
                }
            }
        }

    }

    private static Token toToken(ResultSet rs) throws SQLException {
        int i = 1;
        Token token = new Token();
        token.setId(rs.getString(i++));
        token.setRequestorId(rs.getString(i++));
        token.setExpiration(rs.getTimestamp(i++).toLocalDateTime());
        return token;
    }

    private Timestamp getSqlTimestamp(Token token) {
        return Timestamp.valueOf(token.getExpiration());
    }

}
