package com.lagomsoft.pearldiver.engine;

import com.google.common.base.MoreObjects;
import com.lagomsoft.pearldiver.engine.categories.Category;

import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

public class ItemCriteria {
    private final static OrderPatternBuilder orderPatternBuilder = new OrderPatternBuilder();
    private final Category category;
    private final Pattern pattern;
    private final String query;
    private final Integer minPrice;
    private final Integer maxPrice;
    private final Boolean includeDelivery;
    private final ItemCondition itemCondition;
    private final OfferType offerType;

    public ItemCriteria(Category category, String query,
            Integer minPrice, Integer maxPrice, Boolean includeDelivery,
            ItemCondition itemCondition, OfferType offerType) {

        this.category = category;
        this.pattern = query != null ?
                orderPatternBuilder.build(query.toLowerCase(polishLocale))
                : null;
        this.query = query;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.includeDelivery = includeDelivery;
        this.itemCondition = itemCondition;
        this.offerType = offerType;
    }

    public Optional<Category> getCategory() {
        return Optional.ofNullable(category);
    }

    public String getQuery() {
        return query;
    }

    public Optional<Integer> getMinPrice() {
        return Optional.ofNullable(minPrice);
    }

    public Optional<Integer> getMaxPrice() {
        return Optional.ofNullable(maxPrice);
    }

    public Optional<Boolean> getIncludeDelivery() {
        return Optional.ofNullable(includeDelivery);
    }

    public Optional<ItemCondition> getItemCondition() {
        return Optional.ofNullable(itemCondition);
    }

    public Optional<OfferType> getOfferType() {
        return Optional.ofNullable(offerType);
    }

    public boolean matches(Item item) {
        if (offerType != null && !item.getOfferTypes().contains(offerType)) {
            return false;
        }

        if (itemCondition != null
                && item.getItemCondition().isPresent() && item.getItemCondition().get() != itemCondition) {
            return false;
        }
        if (minPrice != null && priceToCheck(item) <= minPrice) {
            return false;
        }

        if (maxPrice != null && priceToCheck(item) > maxPrice) {
            return false;
        }

        if (pattern != null) {
            return pattern.matcher(item.getNameLowercase()).matches();
        }
        // every item meets empty criteria
        return true;
    }

    private float priceToCheck(Item item) {
        return itemPriceToCheck(item) + deliveryPriceToCheck(item);
    }

    private float itemPriceToCheck(Item item) {
        if (offerType == null) {
            return item.getAuctionPrice().isPresent()
                    ? item.getAuctionPrice().get()
                    : item.getBuyNowPrice().orElse(0.0F);
        } else if (offerType == OfferType.AUCTION) {
            return item.getAuctionPrice().orElse(0.0F);
        } else {
            return item.getBuyNowPrice().orElse(0.0F);
        }
    }

    private float deliveryPriceToCheck(Item item) {
        return includeDelivery != null && includeDelivery
                ? item.getMinDeliveryPrice() : 0.0F;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return toStringHelper().toString();
    }

    public MoreObjects.ToStringHelper toStringHelper() {
        return MoreObjects.toStringHelper(this)
                .add("category", category)
                .add("query", query)
                .add("maxPrice", maxPrice)
                .add("includeDelivery", includeDelivery)
                .add("itemCondition", itemCondition)
                .add("offerType", offerType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ItemCriteria itemCriteria = (ItemCriteria) o;

        //noinspection OverlyComplexBooleanExpression
        return compareCategories(category, itemCriteria.getCategory().get())
                && Objects.equals(query, itemCriteria.query)
                && Objects.equals(maxPrice, itemCriteria.maxPrice)
                && Objects.equals(minPrice, itemCriteria.minPrice)
                && Objects.equals(includeDelivery, itemCriteria.includeDelivery)
                && Objects.equals(itemCondition, itemCriteria.itemCondition)
                && Objects.equals(offerType, itemCriteria.offerType);
    }

    private boolean compareCategories(Category c1, Category c2) {
        boolean bothNullOrSame = c1 == c2;
        boolean neitherNull = c1 != null && c2 != null;
        return bothNullOrSame || (neitherNull && c1.getId() == c2.getId());
    }

    @Override
    public int hashCode() {
        int result = category.getId() ^ (category.getId() >>> 16);
        result = 31 * result + Objects.hashCode(query);
        result = 31 * result + Objects.hashCode(maxPrice);
        result = 31 * result + Objects.hashCode(minPrice);
        result = 31 * result + Objects.hashCode(includeDelivery);
        result = 31 * result + Objects.hashCode(itemCondition);
        result = 31 * result + Objects.hashCode(offerType);
        return result;
    }

    public static class Builder {
        private Category category;
        private String query;
        private Integer maxPrice;
        private Integer minPrice;
        private Boolean includeDelivery;
        private ItemCondition itemCondition;
        private OfferType offerType;

        private Builder() {
        }

        public Builder withMaxPrice(Integer maxPrice) {
            this.maxPrice = maxPrice;
            return this;
        }

        public Builder withMinPrice(Integer minPrice) {
            this.minPrice = minPrice;
            return this;
        }

        public Builder withIncludeDelivery(Boolean includeDelivery) {
            this.includeDelivery = includeDelivery;
            return this;
        }

        public Builder withItemCondition(ItemCondition itemCondition) {
            this.itemCondition = itemCondition;
            return this;
        }

        public Builder withOfferType(OfferType offerType) {
            this.offerType = offerType;
            return this;
        }

        public Builder withCategory(Category category) {
            this.category = category;
            return this;
        }

        public Builder withQuery(String query) {
            this.query = query;
            return this;
        }

        public ItemCriteria build() {
            return new ItemCriteria(category, query, maxPrice, minPrice, includeDelivery,
                    itemCondition, offerType);
        }
    }
}

