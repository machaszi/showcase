package com.lagomsoft.pearldiver.engine;

import com.lagomsoft.pearldiver.engine.categories.Category;

import javax.annotation.Nonnull;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

public class Order {
    private final Long id;
    private final long userId;
    private final ItemCriteria itemCriteria;

    public Order(Long id, long userId, Category category, @Nonnull String query,
            Integer minPrice, Integer maxPrice, Boolean includeDelivery,
            ItemCondition itemCondition, OfferType offerType) {
        Objects.requireNonNull(query);
        Objects.requireNonNull(category);

        this.id = id;
        this.userId = userId;
        itemCriteria = new ItemCriteria(category, query, minPrice, maxPrice, includeDelivery,
                itemCondition, offerType);
    }

    public Long getId() {
        return id;
    }

    public Category getCategory() {
        return itemCriteria.getCategory().get();
    }

    public long getUserId() {
        return userId;
    }

    public String getQuery() {
        return itemCriteria.getQuery();
    }

    public Optional<Integer> getMinPrice() {
        return itemCriteria.getMinPrice();
    }

    public Optional<Integer> getMaxPrice() {
        return itemCriteria.getMaxPrice();
    }

    public Optional<Boolean> getIncludeDelivery() {
        return itemCriteria.getIncludeDelivery();
    }

    public Optional<ItemCondition> getItemCondition() {
        return itemCriteria.getItemCondition();
    }

    public Optional<OfferType> getOfferType() {
        return itemCriteria.getOfferType();
    }

    public boolean matches(Item item) {
        return itemCriteria.matches(item);
    }

    public static Builder builder(long userId, Category category, String query) {
        return new Builder(userId, category, query);
    }

    @Override
    public String toString() {
        return itemCriteria.toStringHelper()
                .add("id", id)
                .add("userId", userId)
        .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Order order = (Order) o;

        return Objects.equals(id, order.id)
                && userId == order.userId
                && itemCriteria.equals(order.itemCriteria);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(itemCriteria);
        result = 31 * result + Objects.hashCode(id);
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        return result;
    }

    public static class Builder {
        private Long id;
        private final long userId;
        private final Category category;
        private final String query;
        private Integer minPrice;
        private Integer maxPrice;
        private Boolean includeDelivery;
        private ItemCondition itemCondition;
        private OfferType offerType;

        private Builder(long userId, Category category, String query) {
            this.userId = userId;
            this.category = category;
            this.query = query;
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withMinPrice(Integer minPrice) {
            this.minPrice = minPrice;
            return this;
        }

        public Builder withMaxPrice(Integer maxPrice) {
            this.maxPrice = maxPrice;
            return this;
        }

        public Builder withIncludeDelivery(Boolean includeDelivery) {
            this.includeDelivery = includeDelivery;
            return this;
        }

        public Builder withItemCondition(ItemCondition itemCondition) {
            this.itemCondition = itemCondition;
            return this;
        }

        public Builder withOfferType(OfferType offerType) {
            this.offerType = offerType;
            return this;
        }

        public Order build() {
            return new Order(id, userId, category, query, minPrice,
                    maxPrice, includeDelivery, itemCondition, offerType);
        }
    }
}
