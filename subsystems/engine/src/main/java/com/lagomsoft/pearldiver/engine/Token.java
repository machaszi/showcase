package com.lagomsoft.pearldiver.engine;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.Base64;

public class Token {

    private String id;
    private String requestorId;
    private LocalDateTime expiration;

    private static SecureRandom secureRandom;

    private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder();

    static {
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public Token() {

    }

    public Token(String requestorId, LocalDateTime expiration) {
        byte[] bytes = new byte[15];
        secureRandom.nextBytes(bytes);
        this.id = base64Encoder.encodeToString(bytes);
        this.requestorId = requestorId;
        this.expiration = expiration;
    }

    public Token(String requestorId, long expirationTime) {
        this(requestorId, getExpiration(expirationTime));
    }

    private static LocalDateTime getExpiration(long expirationTime) {
        return LocalDateTime.now().plusSeconds(expirationTime);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequestorId() {
        return requestorId;
    }

    public void setRequestorId(String requestorId) {
        this.requestorId = requestorId;
    }

    public LocalDateTime getExpiration() {
        return expiration;
    }

    public void setExpiration(LocalDateTime expiration) {
        this.expiration = expiration;
    }

    public boolean isExpired() {
        return LocalDateTime.now().compareTo(expiration) > 0;
    }
}
