package com.lagomsoft.pearldiver.engine;

import com.lagomsoft.pearldiver.engine.categories.Category;
import com.lagomsoft.pearldiver.engine.connectors.ItemSourceId;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Collections;

public class OrderTest {
    private static final int USER_ID = 1;
    private static final String QUERY = "samsung galaxy";
    private static final String ITEM_NAME = "samsung galaxy";
    private static final String ITEM_LINK = "http://host.com/item";
    private static final Category TEST_CATEGORY =
            new Category(1, Collections.singletonList("test"), Collections.singletonList(1));

    @DataProvider
    public static Object[][] orderMatches() {
        return new Object[][] {
            {
                order().build(),
                item(100.0F, null),
                true
            },
            {
                order().build(),
                item(null, 100.0F),
                true
            },
            {
                order().build(),
                item(30.0F, 100.0F),
                true
            },
            {
                order().build(),
                item(100.0F, null, ItemCondition.USED),
                true
            },
            {
                order().withMaxPrice(80).build(),
                item(100.0F, null),
                false
            },
            {
                order().withMaxPrice(80).build(),
                item(50.0F, null),
                true
            },
            {
                order().withMaxPrice(80).build(),
                item(null, 100.0F),
                false
            },
            {
                order().withMaxPrice(80).build(),
                    item(null, 50.0F),
                true
            },
            {
                order().withMaxPrice(80).build(),
                item(50.0F, 100.0F),
                true
            },
            {
                order().withMaxPrice(80).build(),
                item(50.0F, 60.0F),
                true
            },
            {
                order().withMaxPrice(80).build(),
                item(100.0F, 120.0F),
                false
            },
            {
                order().withItemCondition(ItemCondition.NEW).build(),
                item(100.0F, null, ItemCondition.NEW),
                true
            },
            {
                order().withItemCondition(ItemCondition.USED).build(),
                item(100.0F, null, ItemCondition.NEW),
                false
            },
            {
                order().withItemCondition(ItemCondition.USED).build(),
                item(100.0F, null, ItemCondition.USED),
                true
            },
            {
                order().withItemCondition(ItemCondition.NEW).build(),
                item(100.0F, null, ItemCondition.USED),
                false
            },
            {
                order().withOfferType(OfferType.AUCTION).build(),
                item(100.0F, null),
                true
            },
            {
                order().withOfferType(OfferType.AUCTION).build(),
                item(null, 100.0F),
                false
            },
            {
                order().withOfferType(OfferType.BUY_NOW).build(),
                item(100.0F, null),
                false
            },
            {
                order().withOfferType(OfferType.BUY_NOW).build(),
                    item(null, 100.0F),
                true
            },
            {
                order().withMaxPrice(100).build(),
                item(90.0F, null, 20),
                true
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).build(),
                item(90.0F, null, 20),
                false
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).build(),
                item(50.0F, null, 20),
                true
            },
            {
                order().withMaxPrice(100).build(),
                item(null, 90.0F, 20),
                true
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).build(),
                item(null, 90.0F, 20),
                false
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).build(),
                item(null, 50.0F, 20),
                true
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).build(),
                item(50.0F, 90.0F, 20),
                true
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).build(),
                item(90.0F, 120.0F, 20),
                false
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).withOfferType(OfferType.AUCTION).build(),
                item(null, 50.0F, 20),
                false
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).withOfferType(OfferType.AUCTION).build(),
                item(50.0F, null, 20),
                true
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).withOfferType(OfferType.AUCTION).build(),
                item(100.0F, null, 20),
                false
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(false).withOfferType(OfferType.AUCTION).build(),
                item(100.0F, null, 20),
                true
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).withOfferType(OfferType.BUY_NOW).build(),
                item(null, 50.0F, 20),
                true
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).withOfferType(OfferType.BUY_NOW).build(),
                item(null, 100.0F, 20),
                false
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(false).withOfferType(OfferType.BUY_NOW).build(),
                item(null, 100.0F, 20),
                true
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).withOfferType(OfferType.BUY_NOW).build(),
                item(50.0F, null, 20),
                false
            },
            {
                order().withMaxPrice(100).withIncludeDelivery(true).withOfferType(OfferType.BUY_NOW).build(),
                item(50.0F, 100.0F, 20),
                false
            },
            {
                order().withMinPrice(119).withIncludeDelivery(true).withOfferType(OfferType.BUY_NOW).build(),
                item(50.0F, 100.0F, 20),
                true
            },
            {
                order().withMinPrice(121).withIncludeDelivery(true).withOfferType(OfferType.BUY_NOW).build(),
                item(50.0F, 100.0F, 20),
                false
            },
            {
                order().withMinPrice(120).withIncludeDelivery(true).withOfferType(OfferType.BUY_NOW).build(),
                item(50.0F, 100.0F, 20),
                false
            },
            {
                order().withMinPrice(100).withIncludeDelivery(false).withOfferType(OfferType.BUY_NOW).build(),
                item(50.0F, 100.0F, 20),
                false
            },
            {
                order().withMinPrice(99).withIncludeDelivery(false).withOfferType(OfferType.BUY_NOW).build(),
                item(50.0F, 100.0F, 20),
                true
            },
            {
                order().withMinPrice(69).withIncludeDelivery(true).withOfferType(OfferType.AUCTION).build(),
                item(50.0F, 100.0F, 20),
                true
            },
            {
                order().withMinPrice(71).withIncludeDelivery(true).withOfferType(OfferType.AUCTION).build(),
                item(50.0F, 100.0F, 20),
                false
            },
            {
                order().withMinPrice(70).withIncludeDelivery(true).withOfferType(OfferType.AUCTION).build(),
                item(50.0F, 100.0F, 20),
                false
            },
            {
                order().withMinPrice(50).withIncludeDelivery(false).withOfferType(OfferType.AUCTION).build(),
                item(50.0F, 100.0F, 20),
                false
            },
            {
                order().withMinPrice(49).withIncludeDelivery(false).withOfferType(OfferType.AUCTION).build(),
                item(50.0F, 100.0F, 20),
                true
            },
            {
                Order.builder(USER_ID, TEST_CATEGORY, "mura plus")
                        .withItemCondition(ItemCondition.USED).build(),
                new Item(ItemSourceId.ALLEGRO, "Wózek Maxi Cosi 3w1, Mura 3 plus", ITEM_LINK, null,
                        1500.0F, 60, ItemCondition.USED, Collections.emptySet()),
                true
            }
        };
    }

    private static Order.Builder order() {
        return Order.builder(USER_ID, TEST_CATEGORY, QUERY);
    }

    private static Item item(Float auctionPrice, Float buyNowPrice,
            float minDeliveryPrice, ItemCondition itemCondition) {
        return new Item(ItemSourceId.ALLEGRO, ITEM_NAME, ITEM_LINK, auctionPrice, buyNowPrice, minDeliveryPrice,
                itemCondition, Collections.emptySet());
    }

    private static Item item(Float auctionPrice, Float buyNowPrice) {
        return item(auctionPrice, buyNowPrice, 0, ItemCondition.NEW);
    }

    private static Item item(Float auctionPrice, Float buyNowPrice, float minDeliveryPrice) {
        return item(auctionPrice, buyNowPrice, minDeliveryPrice, ItemCondition.NEW);
    }

    private static Item item(Float auctionPrice, Float buyNowPrice, ItemCondition itemCondition) {
        return item(auctionPrice, buyNowPrice, 0, itemCondition);
    }

    @Test(dataProvider = "orderMatches")
    public void shouldMatchOrdersWithItems(
            Order order, Item item, boolean result) throws Exception {
        OrderAssert.assertThat(order).matches(item, result);
    }


}
