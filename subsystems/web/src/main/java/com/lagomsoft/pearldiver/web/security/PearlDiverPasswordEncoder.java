package com.lagomsoft.pearldiver.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Service
public class PearlDiverPasswordEncoder implements PasswordEncoder {
    private static final Logger logger = LoggerFactory.getLogger(PearlDiverPasswordEncoder.class);
    private static final int BCRYPT_PASSWORD_STRENGTH = 15;
    private static final BCryptPasswordEncoder bCryptPasswordEncoder =
            new BCryptPasswordEncoder(BCRYPT_PASSWORD_STRENGTH);


    @Override
    public String encode(CharSequence rawPassword) {
        return bCryptPasswordEncoder.encode(rawPassword);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        boolean isBCrypt = bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
        boolean isOldAlgorithm = false;
        if (!isBCrypt) {
            try {
                isOldAlgorithm = encodedPassword.equals(encodePassword(rawPassword.toString()));
            } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
                logger.error("An exception occurred while encoding password", e);
            }
        }
        return isBCrypt || isOldAlgorithm;
    }

    private static String encodePassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(password.getBytes("UTF-8"));
        return Base64.getEncoder().encodeToString(hash);
    }
}
