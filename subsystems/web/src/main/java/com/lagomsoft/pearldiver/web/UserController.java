package com.lagomsoft.pearldiver.web;

import com.lagomsoft.pearldiver.web.security.PearlDiverUser;
import com.lagomsoft.pearldiver.engine.Token;
import com.lagomsoft.pearldiver.engine.persistence.ConnectionFactory;
import com.lagomsoft.pearldiver.engine.persistence.TokenDao;
import com.lagomsoft.pearldiver.engine.persistence.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserController {

    private static final long NEW_USER_ID = 0;
    private static final String ERROR = "error";
    private static final String SUCCESS = "success";
    private static final String TOKEN_EXPIRED_CODE = "tokenExpired";
    private static final String TOKEN_EXPIRED_MESSAGE = "Token has expired.";
    private static final String PASSWORD_UPDATE_ERROR_CODE = "Current password is invalid.";
    private static final String SQL_EXCEPTION_CODE = "sqlException";
    private static final String SQL_EXCEPTION_MESSAGE = "Database error.";

    private static final String UPDATE_PASSWORD_ERROR_MESSAGE =
            "Can't update password. Wrong current password provided.";
    private static final long TOKEN_EXPIRATION_TIME = 30 * 60; //min * sec
    private static final String DEFAULT_REDIRECT = "redirect:/orders";
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final PasswordEncoder passwordEncoder;
    private final DataSource dataSource;

    @Autowired
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public UserController(PasswordEncoder passwordEncoder, DataSource dataSource) {
        this.passwordEncoder = passwordEncoder;
        this.dataSource = dataSource;
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public String edit(ModelMap model, @PathVariable long userId,
                       @AuthenticationPrincipal User activeUser) {
        PearlDiverUser user = (userId != NEW_USER_ID)
                ? (PearlDiverUser) activeUser
                : new PearlDiverUser(NEW_USER_ID);

        model.addAttribute("user", user);
        return "users/edit_user";
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.POST)
    public String save(HttpServletRequest req, @PathVariable long userId,
                       @AuthenticationPrincipal User activeUser) {
        ConnectionFactory connectionFactory = new DataSourceConnectionFactory(dataSource);
        UserDao userDao = new UserDao(connectionFactory);

        try {
            if (userId == NEW_USER_ID) {
                com.lagomsoft.pearldiver.engine.User newUser =
                        user(NEW_USER_ID, req);
                long newUserId = userDao.create(newUser);
                newUser.setId(newUserId);
                setAuthentication(newUser);
            } else if (userId == ((PearlDiverUser) activeUser).getId()) {
                userDao.update(user(userId, req));
                com.lagomsoft.pearldiver.engine.User updatedUser = userDao.findById(userId);
                setAuthentication(updatedUser);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return DEFAULT_REDIRECT;
    }

    @RequestMapping(value = "/{userId}/password", method = RequestMethod.GET)
    public String editPassword(ModelMap model, @PathVariable long userId,
            @AuthenticationPrincipal User activeUser) {
        PearlDiverUser user = (PearlDiverUser) activeUser;

        model.addAttribute("user", user);
        return "users/change_password";
    }

    @RequestMapping(value = "/{userId}/password", method = RequestMethod.PUT)
    public String updatePassword(HttpServletRequest req, @PathVariable long userId,
            @AuthenticationPrincipal User activeUser, ModelMap model,
            RedirectAttributes redirectAttributes) {
        ConnectionFactory connectionFactory = new DataSourceConnectionFactory(dataSource);
        UserDao userDao = new UserDao(connectionFactory);
        PearlDiverUser pearldiverUser = (PearlDiverUser) activeUser;

        try {
            if (userId == ((PearlDiverUser) activeUser).getId()) {
                com.lagomsoft.pearldiver.engine.User user = userDao.findById(userId);
                String currentPassword = req.getParameter("currentPassword");
                String newPassword = req.getParameter("newPassword");
                String encodedNewPassword = passwordEncoder.encode(
                        newPassword != null ? newPassword : "password");
                if (passwordEncoder.matches(currentPassword, user.getPassword())) {
                    user.setPassword(encodedNewPassword);
                    userDao.updatePassword(user);
                    com.lagomsoft.pearldiver.engine.User updatedUser = userDao.findById(userId);
                    setAuthentication(updatedUser);
                } else {
                    logger.info(
                            String.format("Invalid password during password update for userID %d",
                                    user.getId())
                    );
                    model.addAttribute("status", ERROR);
                    model.addAttribute("statusCode", PASSWORD_UPDATE_ERROR_CODE);
                    model.addAttribute("statusMessage", UPDATE_PASSWORD_ERROR_MESSAGE);
                    model.addAttribute("user", pearldiverUser);
                    return "users/change_password";
                }
            }
        } catch (SQLException e) {
            logger.error("Error during password update.", e);
            model.addAttribute("status", ERROR);
            model.addAttribute("statusCode", SQL_EXCEPTION_CODE);
            model.addAttribute("statusMessage", SQL_EXCEPTION_MESSAGE);
            model.addAttribute("user", pearldiverUser);
            return "users/change_password";
        }
        redirectAttributes.addFlashAttribute("status", SUCCESS);
        return DEFAULT_REDIRECT;
    }

    @RequestMapping(value = "/password_reset", method = RequestMethod.GET)
    public String resetPassword(HttpServletRequest req, ModelMap model,
                                @RequestParam(value = "token", required = false) String token) {
        if (!StringUtils.isEmpty(req.getParameter("token"))) {
            model.addAttribute("token", token);
            return "users/password_reset_action";
        }
        return "users/password_reset";
    }

    @RequestMapping(value = "/password_reset", method = RequestMethod.POST)
    public String createResetPasswordRequest(HttpServletRequest req, ModelMap model) {
        ConnectionFactory connectionFactory = new DataSourceConnectionFactory(dataSource);
        UserDao userDao = new UserDao(connectionFactory);
        TokenDao tokenDao = new TokenDao(connectionFactory);
        String email = req.getParameter("email");
        com.lagomsoft.pearldiver.engine.User user;
        try {
            final Optional<com.lagomsoft.pearldiver.engine.User> userOptional =
                    userDao.findByEmail(email);
            if (userOptional.isPresent()) {
                user = userOptional.get();
                Token token = new Token(Long.toString(user.getId()), TOKEN_EXPIRATION_TIME);
                tokenDao.create(token);
                EmailUtils.sendPasswordResetLink(user, token);
                model.addAttribute("status", SUCCESS);
            } else {
                throw new RuntimeException("No user exists for given e-mail address.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(
                    String.format("Failed to create a password reset token for: %s.", email), e);
        }

        return "login";
    }

    @RequestMapping(value = "/password_reset", method = RequestMethod.PUT)
    public String resetPassword(HttpServletRequest req, ModelMap model) {
        ConnectionFactory connectionFactory = new DataSourceConnectionFactory(dataSource);
        TokenDao tokenDao = new TokenDao(connectionFactory);
        UserDao userDao = new UserDao(connectionFactory);
        String password = req.getParameter("password");
        String tokenId = req.getParameter("token");
        com.lagomsoft.pearldiver.engine.User user;

        try {
            Token token = tokenDao.findById(tokenId);
            if (token.isExpired()) {
                logger.info("Token expired");
                model.addAttribute("status", ERROR);
                model.addAttribute("statusCode", TOKEN_EXPIRED_CODE);
                model.addAttribute("statusMessage", TOKEN_EXPIRED_MESSAGE);
            } else {
                String userId = token.getRequestorId();
                user = userDao.findById(Long.parseLong(userId));
                String encodedNewPassword = passwordEncoder.encode(password);
                user.setPassword(encodedNewPassword);
                userDao.updatePassword(user);
                model.addAttribute("status", SUCCESS);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to reset password.", e);
        }

        return "login";
    }
}

